//
//  SUVMMFaker.m
//
//  (c) Czo 2017.

#import "SUVMMFaker.h"

#include <stdio.h>
#include <objc/runtime.h>
#include <Foundation/Foundation.h>
#include <CoreFoundation/CoreFoundation.h>
#include <AppKit/AppKit.h>

static IMP sOriginalImp = NULL;

@implementation SUVMMFaker

+(void)load
{
    Class originalClass = NSClassFromString(@"SUCatalogDataManager");
    Method originalMeth = class_getInstanceMethod(originalClass, @selector(retrieveDistributionDataForProduct:preferredLocalizations:withHandler:));
    sOriginalImp = method_getImplementation(originalMeth);
    
    Method replacementMeth = class_getInstanceMethod(NSClassFromString(@"SUVMMFaker"), @selector(patchedRetrieveDistributionDataForProduct:preferredLocalizations:withHandler:));
    method_exchangeImplementations(originalMeth, replacementMeth);

    NSLog(@"Czo, Julian Fairfax's SWU Patch registered to class: %@. (c)Czo 2017, Julian Fairfax 2020 -=- czo@czo.hu, julianfairfax.gitlab.io", originalClass);
}

-(void)patchedRetrieveDistributionDataForProduct:(id)arg1 preferredLocalizations:(id)arg2 withHandler:(retrieveDistributionDataForProductBlock)arg3 {
    NSLog(@"almafa1: %@, %@, %@", arg1, arg2, arg3);
    sOriginalImp(self, @selector(retrieveDistributionDataForProduct:preferredLocalizations:withHandler:), arg1, arg2, ^(NSObject *url, NSObject *content, NSObject *error ) {
        NSString* standard_xml_content = [[NSString alloc] initWithData:(NSData *)content encoding:NSUTF8StringEncoding];
        NSString* modified_xml_content_VMM_HACK = [standard_xml_content stringByReplacingOccurrencesOfString:@"\"VMM\"" withString:@"\"FPU\""];
        
        NSString* modified_xml_content_HIGH_SIERRA_HACK = [modified_xml_content_VMM_HACK stringByReplacingOccurrencesOfString:@"affects-system-files=\"true\"" withString:@" "];
        NSString* modified_xml_content_MOJAVE_HACK = [modified_xml_content_HIGH_SIERRA_HACK stringByReplacingOccurrencesOfString:@"my.target.filesystem &amp;&amp; my.target.filesystem.type == \'hfs\'" withString:@"1 == 0"];

        NSArray *brokenByLines=[modified_xml_content_MOJAVE_HACK componentsSeparatedByString:@"\n"];
        NSMutableArray* new_array = [NSMutableArray array];

        for ( id str in brokenByLines ) {
            if ( ( ![str containsString:@"10.13.RecoveryHDUpdate"] ) && ( ![str containsString:@"EmbeddedOSFirmware"] ) && ( ![str containsString:@"FirmwareUpdate"] ) ) {
                [new_array addObject:str];
            }
        }

        NSString *combinedStuff = [new_array componentsJoinedByString:@"\n"];
        NSData*  modified_content = [combinedStuff dataUsingEncoding:NSUTF8StringEncoding];
        arg3(url, modified_content, error);
    });
}

@end
