# macOS Patcher
macOS Patcher is an [open source](#license) command line tool for running macOS Sierra, macOS High Sierra, macOS Mojave, and macOS Catalina on unsupported Macs

If you have a Mac that doesn't support Internet Recovery, and you wanted to directly install the latest version of macOS supported by macOS Patcher, you can download a patched copy of macOS 10.15.7 Catalina as a disc image you can put on a USB from this link:
- [10.15.7 Catalina Patched disc image](https://archive.org/details/apple-macos-catalina-patched-iso)

As of version 3.6.1, no changes have been made to the code itself for devices other than the Xserve2,1.

## Linux makes Macs better
If you can, you shoud really consider [using Linux](https://julianfairfax.gitlab.io/blog/20221029/linux-makes-macs-better) instead of this patcher.

## Catalina Unus
Catalina Unus is a command line tool for running macOS Catalina on one HFS or APFS volume. It's integrated into macOS Patcher so you if you have a Mac supported by it, you can create your patched installer using Catalina Unus, and then add macOS Patcher's patches to it by selecting the "Add patches to patched Catalina Unus installer" option.

It's highly recommended to backup your Mac, do a clean install, and migrate the data during setup, cause Catalina Unus has trouble with Catalina's data migration process.

Catalina Unus may not seem to work properly on newer versions of macOS Catalina due to Apple's changes but the patching process should still work. If the installation fails, restart your Mac and follow the patching steps.

## Software Updates
When installing software updates on your unsupported Mac, this patcher's patches may be overwritten by the new versions of system files that are being installed. It's highly recommended to have a patched installer to patch your system after the update.

## Apple Apps on Non-Metal Macs
If your Mac has a non-Metal graphics card, you may not be able to use certain Apple apps. If this is the case, you can download the latest fully functional versions of these apps for your non-Metal Mac from [the Internet Archive](https://archive.org/details/apple-apps-for-non-metal-macs), thanks to [mariobrostech](https://github.com/mariobrostech).

## APFS Firmware Update
If you'd like to install macOS 10.15 Catalina on a Mac that supports macOS 10.13 High Sierra but hasn't been updated it, you need to download and install [this package](https://gateway.ipfs.io/ipfs/QmZ5KmpG4SeHF8gWrHmoLcG9a3BNAcWWQoERg4q2J1kuQL/OfficialAPFSFWUpdate.zip) to install the APFS firmware update.

This update can only be installed if you're running OS X 10.10 Yosemite or newer. You have to reboot after installing the package to apply the firmware update. If your system isn't plugged in to power when installing, the update will not be installed.

If you're not running OS X 10.10 Yosmite or newer, you'll need to download and install macOS 10.13 High Sierra. This can be done by downloading it using [macOS Downloader](https://gitlab.com/julianfairfax/macos-downloader) and creating an installer drive using the createinstallmedia tool.

If you're not sure if your Mac has been updated to High Sierra or not, you can use [this tool](/apfscheck/apfscheck) to check if your Mac supports APFS. This tool only works on Macs that support macOS 10.13 High Sierra. The source code is availabe [here](/apfscheck/apfscheck.c).

## Contributors
I'd like to the thank the following people, and many others, for their research, help, and inspiration.
- [ASentientBot](https://github.com/ASentientBot) for the Mojave and Catalina GeForceTesla graphics acceleration and SkyLight transperancy patches, Catalina graphics acceleration patches (source code in [the Acceleration patches folder](/Acceleration patches)) and force-disabling SIP boot.efi patch, and invaluable research relating to Mojave and Catalina graphics acceleration
- [Czo](https://github.com/czo) for the Sierra to Catalina SUVMMFaker patch (source code in [the SUVVMFaker folder](/SUVMMFaker))
- [dosdude1](https://github.com/dosdude1) for the Catalina installer patches, and invaluable research relating to Mojave and Catalina graphics acceleration
- [jackluke](https://github.com/jacklukem) for invaluable research relating to booting Mojave and Catalina
- [Larsvonhier](https://github.com/larsvonhier) for invaluable research relating to running Mojave on the MacBook4,1
- [Minh-Ton](https://github.com/minh-ton) for invaluable research relating to running the News app on Mojave and Catalina
- [parrotgeek1](https://github.com/parrotgeek1) for the Sierra to Catalina SIPManager (source code in [the SIPManager folder](/SIPManager)), LegacyUSBEthernet, LegacyUSBInjector, LegacyUSBVideoSupport, and MacBook4,1/5,2 Trackpad patches
- [Syncretic](https://github.com/reenigneorcim) for the MouSSE and telemetrap patches
- [mologie](https://github.com/mologie) for the DisableLibraryValidation patch (source code in [this repo](https://github.com/mologie/macos-disable-library-validation))

## Supported Macs

### MacBooks
- MacBook4,1
- MacBook5,1
- MacBook5,2
- MacBook6,1
- MacBook7,1

### MacBook Airs
- MacBookAir2,1
- MacBookAir3,1
- MacBookAir3,2
- MacBookAir4,1
- MacBookAir4,2

### MacBook Pros
- MacBookPro4,1
- MacBookPro5,1
- MacBookPro5,2
- MacBookPro5,3
- MacBookPro5,4
- MacBookPro5,5
- MacBookPro6,1
- MacBookPro6,2
- MacBookPro7,1
- MacBookPro8,1
- MacBookPro8,2
- MacBookPro8,3

### iMacs
- iMac7,1 (with an upgraded C2D CPU only)
- iMac8,1
- iMac9,1
- iMac10,1
- iMac10,2
- iMac11,1
- iMac11,2
- iMac11,3
- iMac12,1
- iMac12,2

### Mac minis
- Macmini3,1
- Macmini4,1
- Macmini5,1
- Macmini5,2
- Macmini5,3

### Mac Pros
- MacPro3,1
- MacPro4,1

### Xserves
- Xserve2,1
- Xserve3,1

## Supported Graphics Cards
- Pre-Metal AMD video cards (Radeon HD 6000 series and older without acceleration, Radeon HD 4000 series and older with acceleration.)
- Pre-Metal Nvidia video cards (GeForce 500 series and older, i.e. 8600M(GT)/8800M(GT), 9400M/9600M(GT), 320M/330M)
- Pre-Metal Intel video cards (Intel HD Graphics 3000 and Intel HD Graphics Arrandale)

## Supported Versions of macOS
- 10.12 Sierra
- 10.13 High Sierra
- 10.14 Mojave
- 10.15 Catalina

## Known Issues

### Graphics Acceleration, Sleep, and Brightness Control

Your MacBook4,1 and Xserve2,1 won't support graphics acceleration with this patcher. Sleep and brightness control also won't work. And applications that rely on your graphics card will perform slower or will simply not work. If you have an Xserve2,1, sleep and brightness control will work fine. The system has been set to not attempt to sleep. If you would like to disable this, run sudo pmset -a disablesleep 0

Affected devices:
- MacBook4,1
- Xserve2,1 (sleep and brightness control unaffected Xserve2,1)

Affected versions of macOS:
- 10.12 Sierra
- 10.13 High Sierra
- 10.14 Mojave
- 10.15 Catalina

### MacBook4,1 Built-in iSight Camera

If you have a MacBook4,1, your built-in iSight camera will not work with this patcher.

Affected devices:
- MacBook4,1

Affected versions of macOS:
- 10.12 Sierra
- 10.13 High Sierra
- 10.14 Mojave
- 10.15 Catalina

### AMD Radeon HD 5000/6000 Series Graphics Card Acceleration

Currently, it is not possible to get full graphics acceleration on an AMD Radeon HD 5000 or 6000 series graphics card. macOS will be almost unusable without graphics acceleration. This includes the 15" and 17" MacBook Pro systems (MacBookPro8,2 and 8,3). If you want to enable GPU acceleration on these machines, you'll need to disable the AMD graphics card (This will work on MacBook Pro 8,2 and 8,3 systems ONLY. You CANNOT disable the AMD GPU in an iMac.) Weird colors will also be produced when running Mojave with one of these graphics cards installed/enabled.

Affected graphics cards:
- AMD Radeon HD 5000 or 6000 series

Affected versions of macOS:
- 10.14 Mojave
- 10.15 Catalina

### All Photos Tab

In the Photos app, there is no tab for "All Photos." However, if you right-click any photo, you can choose, "Show in All Photos" and the expected "All Photos" functionality is displayed.​ You can also set up a smart album, call it "All Photos" and use search criteria "Date Captured is after 1/1/1970".

Affected versions of macOS:
- 10.15 Catalina

## Usage

### Create Installer Drive

#### Step 1

Download the latest version of the patcher from the GitHub releases page.

You can also download it from the Homebrew repository that is now available [here](https://gitlab.com/julianfairfax/package-repo).

#### Step 2

Open Disk Utility and select your installer drive, then click Erase.

#### Step 3

Select Mac OS Extended Journaled and click Erase.

#### Step 4

Unzip the download and open Terminal. Type chmod +x and drag the script file to Terminal, then hit enter. Then type sudo, drag the script file to Terminal, and hit enter.

#### Step 5

Drag your installer app to Terminal. Then select your installer drive from the list and copy and paste the number.

### Erase System Drive (optional)

These Steps are optional. A clean install is recommended but not required. It's recommended to make a Time Machine backup before erasing your system drive.

#### Step 1

Open the Utilities menu and click Disk Utility. Select your system drive and click Erase.

#### Step 2

Select Mac OS Extended Journaled or APFS and click Erase. Don't select APFS if your Mac doesn't support APFS.

### Install the OS

#### Step 1

Boot from your installer drive by holding down the option key when booting and selecting your installer drive from the menu. Then select your language from the list.

#### Step 2

Close Disk Utility if you used it to erase your system drive, then click Continue.

#### Step 3

Select your system drive, the drive to install the OS on, and click Continue.

Wait 35-45 minutes and click Reboot when the installation is complete. Then boot into your installer drive using the previous instructions.

### Patch the OS

This is the important part. This is the part where you install the patcher files onto your system. If you miss this part or forget it then your system won't boot.

#### Step 1

Open the Utilities menu and click Terminal. Type patch and hit enter. Make sure to select the model your drive will be used with. Then select your system drive from the list and copy and paste the number.

Wait 5-10 minutes and reboot when the patch tool has finished patching your system drive. Then boot into your system drive and setup the OS.

### Restore the OS

If you've switched to a new Mac or just want to remove the patcher files from your system, you can run the restore tool from your installer drive.

#### Step 1

Boot from your installer drive by holding down the option key when booting and selecting your installer drive from the menu. Then select your language from the list.

#### Step 2

Open the Utilities menu and click Terminal. Type restore and hit enter. Make sure to select the model you selected when you last patched. Then select your system drive from the list and copy and paste the number.

#### Step 3

You can choose to remove all system patches or the APFS system patch if you have it installed. Don't remove the APFS system patch if your Mac doesn't support APFS.

Wait 5-10 minutes if you selected to remove all system patches, then reinstall the OS. If you selected to remove the APFS system patch, then you don't need to reinstall the OS, and you can boot back into your system drive afterwards

## Docs
If you want to know how macOS Patcher works, you can check [this doc](https://julianfairfax.github.io/docs/patching-macos-to-run-on-unsupported-macs.html) I wrote about patching macOS to run on unsupported Macs and [this one](https://julianfairfax.github.io/docs/patching-macos-to-install-on-unsupported-macs.html) about patching macOS to install on unsupported Macs. You can also review the code yourself and feel free to submit a pull request if you think part of it can be improved.

## License
The following files and folders were created by me and are licensed under the [GNU General Public License v3.0](http://choosealicense.com/licenses/gpl-3.0/):
- macOS Patcher.sh
- apfscheck
- resources/apfsprep.sh
- resources/InstallerMenuAdditions.plist
- resources/openinstallmedia.sh
- resources/patch.sh
- resources/restore.sh
- resources/runpatch.sh
- resources/runrestore.sh
- resources/patch/com.apple.PowerManagement.plist
- resources/patch/com.apple.security.libraryvalidation.plist
- resources/patch/com.julianfairfax.swupost.plist
- resources/patch/swubless.sh
- resources/patch/swuinsta.sh
- resources/patch/swupost.sh

The following files and folders were modified by me and those modificatins are licensed under the [GNU General Public License v3.0](http://choosealicense.com/licenses/gpl-3.0/):
- resources/prelinkedkernel/10.12/prelinkedkernel, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-install-on-unsupported-macs#what-new-patches-do-i-have-to-use-for-this-version-1) from Apple, from 10.12
- resources/prelinkedkernel/10.12.1/prelinkedkernel, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-install-on-unsupported-macs#what-new-patches-do-i-have-to-use-for-this-version-1) from Apple, from 10.12.1
- resources/prelinkedkernel/10.12.4/prelinkedkernel, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-install-on-unsupported-macs#what-new-patches-do-i-have-to-use-for-this-version-1) from Apple, from 10.12.4
- resources/prelinkedkernel/10.13/prelinkedkernel, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-install-on-unsupported-macs#what-new-patches-do-i-have-to-use-for-this-version-1) from Apple, from 10.13
- resources/prelinkedkernel/10.13.4/prelinkedkernel, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-install-on-unsupported-macs#what-new-patches-do-i-have-to-use-for-this-version-1) from Apple, from 10.13.4
- resources/prelinkedkernel/10.14/prelinkedkernel, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-install-on-unsupported-macs#what-new-patches-do-i-have-to-use-for-this-version-2) from Apple, from 10.14
- resources/prelinkedkernel/10.14.1/prelinkedkernel, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-install-on-unsupported-macs#what-new-patches-do-i-have-to-use-for-this-version-2) from Apple, from 10.14.1
- resources/prelinkedkernel/10.14.4/prelinkedkernel, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-install-on-unsupported-macs#what-new-patches-do-i-have-to-use-for-this-version-2) from Apple, from 10.14.4
- resources/prelinkedkernel/10.15/prelinkedkernel, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-install-on-unsupported-macs#what-new-patches-do-i-have-to-use-for-this-version-3) from Apple, from 10.15.4
- resources/patch/atomicupdatetool, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-run-on-unsupported-macs#the-modern-updates-problem) from Apple, from 10.15.7
- resources/patch/startup-update.nsh, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-run-on-unsupported-macs#the-modern-updates-problem) from dosdude1
- resources/patch/SUVMMFaker.dylib, [modified](/SUVMMFaker/SUVMMFaker.m#L37) from Czo
- resources/patch/MacBook4,1/IOUSBHostFamily.kext, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-run-on-unsupported-macs#what-new-patches-do-i-have-to-use-for-this-version-1) from Apple, from 10.10.5
- SUVMMFaker/SUVMMFaker.m, [modified](/SUVMMFaker/SUVMMFaker.m#L37) from Czo

The following files and folders were created by other developers and are licensed under their licenses:
- Acceleration patches, by ASentientBot
- resources/DisableLibraryValidation.kext, by mologie
- resources/patch/BOOTX64.efi, by dosdude1
- resources/patch/SIPManager.kext, by parrotgeek1
- resources/patch/startup.nsh, by dosdude1
- resources/patch/10.12/SUVMMFaker.dylib, by Czo
- SIPManager, by parrotgeek1
- SUVMMFaker, by Czo

The following files and folders were created by other developers and are licensed under their non-free licenses:
- resources/patch/AAAMouSSE.kext, by Syncretic
- resources/patch/telemtrap.kext, by Syncretic

The following files and folders were modified by other developers and those modifications are licensed under their licenses:
- resources/brtool, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-install-on-unsupported-macs#the-modern-installer-problem) by dosdude1, from Apple, from 10.15.4 installer
- resources/OSInstaller, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-install-on-unsupported-macs#the-modern-installer-problem) by dosdude1, from Apple, from 10.15.4 installer
- resources/OSInstallerSetupInternal, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-install-on-unsupported-macs#the-modern-installer-problem) by dosdude1, from Apple, from 10.15.4 installer
- resources/osishelperd, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-install-on-unsupported-macs#the-modern-installer-problem) by dosdude1, from Apple, from 10.15.4 installer
- resources/patch/boot.efi, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-run-on-unsupported-macs#the-library-validation-problem) by ASentientBot, from Apple, from 10.15.4
- resources/patch/com.apple.softwareupdated.plist, [modified](/resources/patch/com.apple.softwareupdated.plist#L69) by Czo, from Apple, from 10.12.6
- resources/patch/CoreDisplay.framework, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-run-on-unsupported-macs#the-non-metal-acceleration-problem) by ASentientBot, from Apple, from 10.14.4 and 10.15.4
- resources/patch/GeForceTesla.kext, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-run-on-unsupported-macs#what-new-patches-do-i-have-to-use-for-this-version-5) by ASentientBot, from Apple
- resources/patch/LegacyUSBEthernet.kext, modified by parrotgeek1, from Apple, from 10.12.6
- resources/patch/LegacyUSBInjector.kext, modified by parrotgeek1, from Apple, from 10.12.6
- resources/patch/LegacyUSBVideoSupport.kext, modified by parrotgeek1, from Apple, from 10.12.6
- resources/patch/SkyLight.framework, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-run-on-unsupported-macs#the-non-metal-acceleration-problem) by ASentientBot, from Apple, from 10.14.6 and 10.15.4
- resources/patch/Trackpad.prefPane, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-run-on-unsupported-macs#what-new-patches-do-i-have-to-use-for-this-version-2) by parrotgeek1, from Apple, from 10.11.6
- resources/patch/10.14/SkyLight.framework, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-run-on-unsupported-macs#what-new-patches-do-i-have-to-use-for-this-version-4) by ASentientBot, from Apple, from 10.14.6

The following files and folders were created by Apple and are licensed under their licenses:
- resources/macOS Installer.app, from 10.12.6 installer
- resources/OSInstaller.framework, from 10.12 beta 1 installer
- resources/Quartz.framework, from 10.12.6 installer
- resources/SystemMigration.framework, from 10.13.6 installer
- resources/SystemMigrationUtils.framework, from 10.13.6 installer
- resources/patch/AirPortAtheros40.kext, from 10.13.6
- resources/patch/AmbientLightSensorHID.plugin, from 10.11.6
- resources/patch/AMD2400Controller.kext, from 10.13.6
- resources/patch/AMD2600Controller.kext, from 10.13.6
- resources/patch/AMD3800Controller.kext, from 10.13.6
- resources/patch/AMD4600Controller.kext, from 10.13.6
- resources/patch/AMD4800Controller.kext, from 10.13.6
- resources/patch/AMD5000Controller.kext, from 10.13.6
- resources/patch/AMD6000Controller.kext, from 10.13.6
- resources/patch/AMDFramebuffer.kext, from 10.13.6
- resources/patch/AMDLegacyFramebuffer.kext, from 10.13.6
- resources/patch/AMDLegacySupport.kext, from 10.13.6
- resources/patch/AMDRadeonVADriver2.bundle, from 10.13.6
- resources/patch/AMDRadeonVADriver.bundle, from 10.13.6
- resources/patch/AMDRadeonX3000GLDriver.bundle, from 10.12.6
- resources/patch/AMDRadeonX3000.kext, from 10.12.6
- resources/patch/AMDRadeonX4000GLDriver.bundle, from 10.12.6
- resources/patch/AMDRadeonX4000HWServices.kext, from 10.13.6
- resources/patch/AMDRadeonX4000.kext, from 10.12.6
- resources/patch/AMDShared.bundle, from 10.13.6
- resources/patch/AMDSupport.kext, from 10.13.6
- resources/patch/AppleBacklightExpert.kext, from 10.12.6
- resources/patch/AppleBacklight.kext, from 10.12.6
- resources/patch/AppleGraphicsControl.kext, from 10.13.6
- resources/patch/AppleGraphicsPowerManagement.kext, from 10.13.6
- resources/patch/AppleGVA.framework, from 10.13.6
- resources/patch/AppleHDA.kext, from 10.11.6
- resources/patch/AppleIntelFramebufferAzul.kext, from 10.13.6
- resources/patch/AppleIntelFramebufferCapri.kext, from 10.13.6
- resources/patch/AppleIntelHD3000GraphicsGA.plugin, from 10.13.6
- resources/patch/AppleIntelHD3000GraphicsGLDriver.bundle, from 10.13.6
- resources/patch/AppleIntelHD3000Graphics.kext, from 10.13.6
- resources/patch/AppleIntelHD3000GraphicsVADriver.bundle, from 10.13.6
- resources/patch/AppleIntelHDGraphicsFB.kext, from 10.13.6
- resources/patch/AppleIntelHDGraphicsGA.plugin, from 10.13.6
- resources/patch/AppleIntelHDGraphicsGLDriver.bundle, from 10.13.6
- resources/patch/AppleIntelHDGraphics.kext, from 10.13.6
- resources/patch/AppleIntelHDGraphicsVADriver.bundle, from 10.13.6
- resources/patch/AppleIntelPIIXATA.kext, from 10.14.6
- resources/patch/AppleIntelSNBGraphicsFB.kext, from 10.13.6
- resources/patch/AppleIntelSNBVA.bundle, from 10.13.6
- resources/patch/AppleMCCSControl.kext, from 10.13.6
- resources/patch/AppleUSBACM.kext, from 10.13.6
- resources/patch/AppleUSBTopCase.kext, from 10.12.6
- resources/patch/AppleYukon2.kext, from 10.14.6
- resources/patch/ATI1300Controller.kext, from 10.7.5
- resources/patch/ATI1600Controller.kext, from 10.7.5
- resources/patch/ATI1900Controller.kext, from 10.7.5
- resources/patch/ATIFramebuffer.kext, from 10.7.5
- resources/patch/ATIRadeonX1000GA.plugin, from 10.7.5
- resources/patch/ATIRadeonX1000GLDriver.bundle, from 10.7.5
- resources/patch/ATIRadeonX1000.kext, from 10.7.5
- resources/patch/ATIRadeonX1000VADriver.bundle, from 10.7.5
- resources/patch/ATIRadeonX2000GA.plugin, from 10.13.6
- resources/patch/ATIRadeonX2000GLDriver.bundle, from 10.13.6
- resources/patch/ATIRadeonX2000.kext, from 10.13.6
- resources/patch/ATIRadeonX2000VADriver.bundle, from 10.13.6
- resources/patch/ATISupport.kext, from 10.7.5
- resources/patch/corecapture.kext, from 10.11.6
- resources/patch/CoreCaptureResponder.kext, from 10.11.6
- resources/patch/DisplayServices.framework, from 10.12.6
- resources/patch/GeForceTeslaGLDriver.bundle, from 10.13.6
- resources/patch/GeForceTeslaVADriver.bundle, from 10.13.6
- resources/patch/GPUSupport.framework, from 10.14.3
- resources/patch/GPUWrangler.framework, from 10.13.6
- resources/patch/IO80211Family.kext, from 10.14.6
- resources/patch/IOAccelerator2D.plugin, from 10.12.6
- resources/patch/IOAcceleratorFamily2.kext, from 10.12.6
- resources/patch/IOAudioFamily.kext, from 10.11.6
- resources/patch/IOGraphicsFamily.kext, from 10.13.6
- resources/patch/IONDRVSupport.kext, from 10.13.6
- resources/patch/IOUSBFamily.kext, from 10.13.6
- resources/patch/IOUSBHostFamily.kext, from 10.13.6
- resources/patch/iSightAudio.driver, from 10.12.6
- resources/patch/JapaneseIM.app, from 10.14.6
- resources/patch/libCoreFSCache.dylib, from 10.15 beta 6
- resources/patch/libmecabra.dylib, from 10.14.6
- resources/patch/MonitorPanel.framework, from 10.15.3
- resources/patch/NVDANV50HalTesla.kext, from 10.13.6
- resources/patch/NVDAResmanTesla.kext, from 10.13.6
- resources/patch/nvenet.kext, from 10.14.6
- resources/patch/OpenGL.framework, from 10.14.3
- resources/patch/SiriUI.framework, from 10.14.3
- resources/patch/TextInput.framework, from 10.14.6
- resources/patch/10.14/CoreDisplay.framework, from 10.14.4
- resources/patch/Broadcom/IO80211Family.kext, from 10.11.6
- resources/patch/MacBook4,1/AppleHDA.kext, from 10.7.5
- resources/patch/MacBook4,1/AppleHIDMouse.kext, from 10.10.5
- resources/patch/MacBook4,1/AppleHSSPIHIDDriver.kext, from 10.11.6
- resources/patch/MacBook4,1/AppleIntelGMAX3100FB.kext, from 10.6.2 beta 1
- resources/patch/MacBook4,1/AppleIntelGMAX3100GA.plugin, from 10.6.2 beta 1
- resources/patch/MacBook4,1/AppleIntelGMAX3100GLDriver.bundle, from 10.6.2 beta 1
- resources/patch/MacBook4,1/AppleIntelGMAX3100.kext, from 10.6.2 beta 1
- resources/patch/MacBook4,1/AppleIntelGMAX3100VADriver.bundle, from 10.6.2 beta 1
- resources/patch/MacBook4,1/AppleIRController.kext, from 10.7.5
- resources/patch/MacBook4,1/AppleMultitouchDriver.kext, from 10.11.6
- resources/patch/MacBook4,1/AppleTopCase.kext, from 10.10.5
- resources/patch/MacBook4,1/AppleUSBMultitouch.kext, from 10.10.5
- resources/patch/MacBook4,1/AppleUSBTopCase.kext, from 10.10.5
- resources/patch/MacBook4,1/IOAudioFamily.kext, from 10.8.4
- resources/patch/MacBook4,1/IOBDStorageFamily.kext, from 10.10.5
- resources/patch/MacBook4,1/IOBluetoothFamily.kext, from 10.10.5
- resources/patch/MacBook4,1/IOBluetoothHIDDriver.kext, from 10.10.5
- resources/patch/MacBook4,1/IOSerialFamily.kext, from 10.10.5
- resources/patch/MacBook4,1/IOUSBFamily.kext, from 10.10.5
- resources/patch/MacBook4,1/IOUSBMassStorageClass.kext, from 10.10.5
- resources/patch/MonitorPanels, from 10.15.3
